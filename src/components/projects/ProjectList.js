import React from 'react';
import ProjectSummary from './ProjectSummary';

// projects này là 1 props
const ProjectList = ({projects}) => {
    console.log({projects});
    return (

        // map là vòng lặp, tương tự như foreach
        // projects là state của projectReducer được truyền từ Dashboard vào đây thông qua props projects
        // Mỗi ProjectSummary sẽ có 1 data tương ứng với project.id
        // Tóm lại: đây là 1 vòng lặp foreach với data truyền từ Dashboard vào.

        <div className="project-list section">
            { projects && projects.map(project => {
                return (
                    <ProjectSummary project={project} key={project.id} />
                )
            })}
        </div>
    );
}

export default ProjectList;

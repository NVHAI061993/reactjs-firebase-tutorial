import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

var firebaseConfig = {
    apiKey: "AIzaSyD1lZmXZ0CWC8-fwTdTHVgYVTp0JE5WOco",
    authDomain: "kawaiicode-dae5c.firebaseapp.com",
    databaseURL: "https://kawaiicode-dae5c.firebaseio.com",
    projectId: "kawaiicode-dae5c",
    storageBucket: "kawaiicode-dae5c.appspot.com",
    messagingSenderId: "870612333926",
    appId: "1:870612333926:web:9b8fe4a155d043cf"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.firestore().settings({ timestampsInSnapshots: true })

export default firebase;

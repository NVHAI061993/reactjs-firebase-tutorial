// createProject lấy hành dộng từ projectActions
// projectActions có nhiều hành động. Mỗi hành động đặt tên là type
// Nói đơn giản: giống services trong Laravel. Một nơi chứa các hành động thêm, sửa, xóa, tìm kiếm

export const createProject = (project) => {
    return(dispatch, getState, { getFirebase, getFirestore }) => {
        // async gọi database
        // project nhận từ CreateProject
        dispatch({ type: 'CREATE_PROJECT', project });
    }
}

import authReducer from './authReducer'
import projectReducer from './projectReducer'
import { combineReducers } from 'redux'

// Hiểu đơn giản, const này bao gồm 2 state của authReducer và projectReducer
const rootReducer = combineReducers({
    auth: authReducer,
    project: projectReducer // rootReducer đã có state của projectReducer
})

export default rootReducer
